//
//  TEPopUp.m
//  TEPopUp
//
//  Created by  on 12/4/19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TEPopUp.h"

@interface TEPopUp (PrivateMethods)
- (void)createWindowWithSize:(CGSize)viewSize;
@end

@implementation TEPopUp
@synthesize dialogView = _dialogView;

- (void)show {
    [window makeKeyAndVisible];
}

- (void)dismiss {
    [window setHidden:YES];
}

- (void)createWindowWithSize:(CGSize)viewSize {
    UIWindow *topWin = (UIWindow *) [[UIApplication sharedApplication].windows lastObject];
    CGRect rect = topWin.frame;
    window = [[UIWindow alloc] initWithFrame:rect];
    
    UIViewController *rootVC = [[UIViewController alloc] init];
    UIView *rootView = [[UIView alloc] initWithFrame:rect];
    [rootVC setView:rootView];
    [window setRootViewController:rootVC];    
    
    // translucent view
    UIView *translucentView = [[UIView alloc] initWithFrame:rect];
    [translucentView setBackgroundColor:[UIColor blackColor]];
    [translucentView setAlpha:0.6f];
    [rootView addSubview:translucentView];
    
    UIImage *image = [UIImage imageNamed:@"global_bg_alert.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(11.0f, 
                                           13.0f, 
                                           image.size.height, 
                                           image.size.width);
    image = [image resizableImageWithCapInsets:insets];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    rect = CGRectMake(0, 0, viewSize.width, viewSize.height);
    [imageView setFrame:rect];
    [imageView setCenter:rootView.center];
    [rootView addSubview:imageView];
    
    // transparent view
    _dialogView = [[UIView alloc] initWithFrame:rect];
    [_dialogView setBackgroundColor:[UIColor clearColor]];
    [_dialogView setCenter:rootView.center];
    [rootView addSubview:_dialogView];
}

- (id)initWithSize:(CGSize)size {
    self = [super init];
    if (self) {
        [self createWindowWithSize:size];
    }
    return self;
}
@end