//
//  TEPopUp.h
//  TEPopUp
//
//  Created by  on 12/4/19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TEPopUp : NSObject
{
@private
    UIWindow *window;
    UIView *_dialogView;
}
@property (nonatomic) UIView *dialogView;
- (void)show;
- (void)dismiss;
- (id)initWithSize:(CGSize)size;
@end
